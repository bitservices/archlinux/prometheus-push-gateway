# Maintainer: Richard Lees <git zero at bitservices dot io>
# Contributor: Felix Golatofski <contact@xdfr.de>
# Contributor: Nils Czernia <nils@czserver.de>
#
# Taken from: https://aur.archlinux.org/packages/prometheus-push-gateway-bin
###############################################################################

pkgname=prometheus-push-gateway
pkgver=1.11.0
pkgrel=1
pkgdesc='Prometheus push acceptor for ephemeral and batch jobs'
arch=('x86_64'
      'arm'
      'armv6h'
      'armv7h'
      'aarch64')
url='https://github.com/prometheus/pushgateway'
license=('Apache')

source=("${pkgname}.conf"
        "${pkgname}.service"
        "${pkgname}.sysusers")
source_x86_64=("https://github.com/prometheus/pushgateway/releases/download/v${pkgver}/pushgateway-${pkgver}.linux-amd64.tar.gz")
source_arm=("https://github.com/prometheus/pushgateway/releases/download/v${pkgver}/pushgateway-${pkgver}.linux-armv5.tar.gz")
source_armv6h=("https://github.com/prometheus/pushgateway/releases/download/v${pkgver}/pushgateway-${pkgver}.linux-armv6.tar.gz")
source_armv7h=("https://github.com/prometheus/pushgateway/releases/download/v${pkgver}/pushgateway-${pkgver}.linux-armv7.tar.gz")
source_aarch64=("https://github.com/prometheus/pushgateway/releases/download/v${pkgver}/pushgateway-${pkgver}.linux-arm64.tar.gz")

sha256sums=('be9f4e2f79402e681c0cd3c4b29be0a409f77a46d343467c3fbd13984222f5ef'
            '8b13f20a2c51bc08d0f5ce00145dcd0c15e4251709a0d4036315e9ef9794fa18'
            '40f36f2a5c81c1fee899bd01d5610a2ecccb06ddb12b0a9585f1795acd397b50')
sha256sums_x86_64=('5888b0c36d1b8e85950b6eb81ad168ff485a139807896a8727f877813690170c')
sha256sums_arm=('9e2378820eee4fbc3040371dcea9d5ac77e0d1ea837b7cf55646f394f2f73e40')
sha256sums_armv6h=('88023c26d3e437dfae00e8ed51f33d6515d36a0dddccc433887ccb8a41082033')
sha256sums_armv7h=('b37208a825ede8e21d0b5241ea8d81e1f013445bff1af9fadd5f776b31923ec1')
sha256sums_aarch64=('4d6faa82083513d60d7450e6867cc6f4f78a892e7f698500ea97772677690b55')

###############################################################################

package() {
    case "$CARCH" in
        "x86_64") ARCH="amd64";;
        "arm") ARCH="armv5";;
        "armv6h") ARCH="armv6";;
        "armv7h") ARCH="armv7";;
        "aarch64") ARCH="arm64";;
    esac

    install -Dm755 "${srcdir}/pushgateway-${pkgver}.linux-${ARCH}/pushgateway" "${pkgdir}/usr/bin/${pkgname}"
    install -Dm644 "${srcdir}/${pkgname}.conf" "${pkgdir}/etc/conf.d/${pkgname}"
    install -Dm644 "${srcdir}/${pkgname}.service" "${pkgdir}/usr/lib/systemd/system/${pkgname}.service"
    install -Dm644 "${srcdir}/${pkgname}.sysusers" "${pkgdir}/usr/lib/sysusers.d/${pkgname}.conf"
}

###############################################################################
